def power(a, n):
    """
    Equivalent to ``a**n``.
    """
    result = 1.0
    while n != 0:
        if n % 2 != 0:
            result *= a
            n -= 1
        a *= a
        n /= 2
    return result


def is_symmetric(m):
    """
    Check if the matrix ``m`` is symmetric.
    
    Parameters
    ==========
    
    m : list of lists of ints
    
    """
    n = len(m)
    for i in range(n):
        for j in range(i+1, n):
            if m[i][j] != m[j][i]:
                return False
    
    return True


def is_prime(n):
    """
    Check if the integer ``n`` is prime.
    """
    for i in range(2, int(n**0.5)):
        if n % i == 0:
            return False
    return True


def prime_divisors(n):
    """
    The list of prime divisors of an integer ``n``.
    """
    divisors = []
    divisor = 2
    while divisor != n:
        if n % divisor == 0:
            n /= divisor
            divisors.append(divisor)
        else:
            divisor += 1
    divisors.append(n)
    return divisors


def gcd(a, b):
    """
    The greatest common divisor of ``a`` and ``b``.
    """
    
    while a!=0 and b!=0:
        if a > b:
            a = a % b
        else:
            b = b % a
    
    return a + b


def xgcd(a, b):
    """
    """
    p, q, r, s, x, y = (1, 0, 0, 1, 0, 0)
    while a!=0 and b!=0:
        if a > b:
            a = a - b
            p = p - r
            q = q - s
        else:
            b = b - a
            r = r - p
            s = s - q
    
    x, y = (p, q) if a!=0 else (r, s)
    
    return a+b, x, y

