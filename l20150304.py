def is_cycle(l):
    """
    Return whether or not l defines a cycle.

    Parameters
    ==========
    l : list of integers

    Examples
    ========
    >>> from l20150304 import is_cycle
    >>> is_cycle([1, 7, 4, 6, 9, 2])
    True
    >>> is_cycle([2, 3, 5, 2, 1])
    False

    """
    if len(l) != sum(list(map(l.count, l))):
        return False

    return True


def transpositions(cycle):
    """
    Convert a cycle defined by a list to a product of transpositions.
    (i_1 i_2 ... i_n) = (i_1 i_n)(i_1 i_{n-1}) ... (i_1 i_2)

    Parameters
    ==========
    cycle : list of integers

    Examples
    ========
    >>> from l20150304 import transpositions
    >>> transpositions([1, 3, 4, 6, 2])
    [[1, 2], [1, 6], [1, 4], [1, 3]]

    """
    if not is_cycle(cycle):
        raise ValueError('Input is not a cycle.')

    result = []

    for i in cycle[:0:-1]:
        result.append([cycle[0], i])

    return result


def _sort_count(A):
   l = len(A)
   if l > 1:
      n = l // 2
      C = A[:n]
      D = A[n:]
      C, c = _sort_count(A[:n])
      D, d = _sort_count(A[n:])
      B, b = _merge_count(C,D)
      return B, b+c+d
   else:
      return A, 0


def _merge_count(A,B):
   count = 0
   M = []
   while A and B:
      if A[0] <= B[0]:
         M.append(A.pop(0))
      else:
         count += len(A)
         M.append(B.pop(0))
   M  += A + B
   return M, count


def sign(permutation):
    """
    Return the signature of a permutation.

    Parameters
    ==========
    permutation : list of integers

    """
    invs = _sort_count(permutation)[1]
    return 1 if invs % 2 else -1


def link(a, b, l):
    if l[b-1] != 0 and b != 0:
        t = l[b-1]
        l[b-1] = 0
        return [b] + link(b, t, l)
    return []


def cycles(permutation):
    """
    Return the cycle decomposition of a permutation.

    Parameters
    ==========
    permutation : list of integers

    Examples
    ========
    >>> from l20150304 import cycles
    >>> cycles([2,1,4,3,5])
    [[1, 2], [3, 4], [5]]

    """
    result = []
    p = list(permutation)

    for i, j in enumerate(p):
        c = link(i+1, j, p)
        if c != []:
            shift = c.index(min(c))
            result.append(c[shift:] + c[:shift])

    return result


def order(permutation):
    """
    Return the order of a permutation.

    Parameters
    ==========
    permutation : list of integers

    Examples
    ========
    >>> from l20150304 import order
    >>> order([2,3,4,5,1])
    5
    >>> order([1,2,4,3,5])
    2

    """
    from sage.all import lcm
    c = cycles(permutation)
    return lcm(list(map(len, c)))


def is_groupoid(table):
    """
    Return whether or not ``table`` is Cayley table, i.e. characterizes
    an operation of a groupoid. The intersection of row ``i`` and column
    ``j`` is considered product ``ij``.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    Examples
    ========
    >>> from l20150304 import is_groupoid
    >>> is_groupoid([[0, 1, 1], [2, 1, 1], [0, 2, 1]])
    True
    >>> is_groupoid([[4, 0, 3], [2, 0, 1], [5, 8, 0]])
    False

    """
    import numpy as np
    t = np.array(table)
    n, k = t.shape
    if n != k:
        return False

    m = t.min()
    M = t.max()

    if m < 0 or M > n-1:
        return False

    return True


def is_associative(table):
    """
    Return whether or not binary operation defined by a Cayley
    multiplication table is associative.
    Used direct verification.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    Examples
    ========
    >>> from l20150304 import is_associative
    >>> is_associative([[0, 0, 0], [0, 1, 2], [0, 0, 0]])
    True

    """
    from itertools import product
    n = len(table)
    for triple in product(range(n), repeat=3):
        i, j, k = triple
        ab_c = table[table[i][j]][k]
        a_bc = table[i][table[j][k]]
        if ab_c != a_bc:
            return False

    return True


def is_commutative(table):
    """
    Return whether or not binary operation defined by a Cayley
    multiplication table is commutative.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    Examples
    ========
    >>> from l20150304 import is_commutative
    >>> is_commutative([[0, 1, 2], [1, 2, 0], [2, 0, 1]])
    True
    >>> is_commutative([[0, 0, 0], [0, 1, 2], [0, 0, 0]])
    False

    """
    n = len(table)
    for i in range(n):
        for j in range(i + 1, n):
            if table[i][j] != table[j][i]:
                return False

    return True


def is_latin_square(table):
    """
    Return whether or not division is always possible.

    References
    ==========
    [1] en.wikipedia.org/wiki/Latin_square_property

    Examples
    ========
    >>> from l20150304 import is_latin_square
    >>> is_latin_square([[0, 1, 2], [2, 0, 1], [1, 2, 0]])
    True
    >>> is_latin_square([[0, 0, 1], [2, 1, 1], [1, 2, 0]])
    False

    """
    n = len(table)
    r = list(range(n))

    for row in table:
        if sorted(row) != r:
            return False

    return True


def is_with_identity(table, identity=False):
    """
    Return whether or not there exist an identity element.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]
    identity : bool
        if ``identity`` is True then return the identity element.

    """
    import numpy as np

    n = len(table)
    m = np.array(table)
    r = list(range(n))

    for i in range(n):
        if m[i, :].tolist() == r and m[:, i].tolist() == r:
            if identity:
                return i
            else:
                return True

    return False


def is_quasigroup(table):
    return is_latin_square(table)


def is_semigroup(table):
    """
    Return whether or not ``table`` is Cayley table, i.e. characterizes
    an operation of a semigroup. The intersection of row ``i`` and column
    ``j`` is considered product ``ij``.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    """
    if is_groupoid(table) and is_associative(table):
        return True

    return False


def is_loop(table):
    """
    Return whether or not ``table`` is Cayley table, i.e. characterizes
    an operation of a loop. The intersection of row ``i`` and column
    ``j`` is considered product ``ij``.

    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    Examples
    ========
    >>> from l20150304 import is_loop
    >>> is_loop([[0, 1], [1, 0]])
    True

    """
    if is_quasigroup(table) and is_with_identity(table):
        return True

    return False


def is_monoid(table):
    """
    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    Examples
    ========
    >>> from l20150304 import is_monoid
    >>> is_monoid([[0, 1], [1, 0]])
    True

    """
    if is_semigroup(table) and is_with_identity(table):
        return True

    return False


def is_group(table):
    """
    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    """
    if is_loop(table) and is_associative(table):
        return True

    if is_monoid(table) and is_latin_square(table):
        return True

    return False


def is_abelian_group(table):
    """
    Parameters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]

    """
    if is_group(table) and is_commutative(table):
        return True

    return False


def image(table, mapping):
    """
    Return an image of a Cayley table defined by ``mapping`` under
    bujection defined by ``mapping``.

    Paramaters
    ==========
    table : array-like, shape=[n, n]
        Binary operation defined by a Cayley table,
        with entries from [0, 1, ... , n-1]
    mapping : list
        Bijection of set [0, 1, ... , n-1].

    Examples
    ========
    >>> from l20150304 import image
    >>> import numpy as np
    >>> table = [[0, 1, 2, 3], [1, 2, 3, 0], [2, 3, 0, 1], [3, 0, 1, 2]]
    >>> np.array(table)
    array([[0, 1, 2, 3],
           [1, 2, 3, 0],
           [2, 3, 0, 1],
           [3, 0, 1, 2]])
    >>> image(table, [2, 0, 3, 1])
    array([[2, 3, 0, 1],
           [3, 2, 1, 0],
           [0, 1, 3, 2],
           [1, 0, 2, 3]])

    """
    import numpy as np
    n = len(table)

    if not is_groupoid(table):
        raise ValueError("``table`` doesn't define a binary operation")

    if sorted(mapping) != list(range(n)):
        raise ValueError("``mapping`` is not bijection")

    result = np.empty([n, n], dtype=int)
    for i in range(n):
        for j in range(n):
            result[i, j] = mapping[table[i][j]]

    result = result[:, mapping]
    result = result[mapping, :]

    return result


def is_isomorphism(permutation, table1, table2):
    """
    Return whether or not ``permutation`` is isomorphism from the groupoid
    with multiplication table defined by ``table1`` to groupoid with
    multiplication table defined by ``table2``.
    """
    import numpy as np
    return np.array_equal(image(table1, permutation), np.array(table2))


def is_isomorphic(table1, table2, mapping=False):
    """
    Return whether or not ``table1`` is isomorphic to ``table2``.

    Parameters
    ==========
    table1, table2 : array-like, shape=[n, n]
    mapping : bool
        if ``mapping`` is True returns an isomorphism

    Examples
    ========
    >>> from l20150304 import is_isomorphic
    >>> import numpy as np
    >>> table1 = np.array([
    ...     [0, 1, 2, 3],
    ...     [1, 2, 3, 0],
    ...     [2, 3, 0, 1],
    ...     [3, 0, 1, 2],
    ... ])
    >>> table2 = np.array([
    ...     [3, 0, 1, 2],
    ...     [0, 1, 2, 3],
    ...     [1, 2, 3, 0],
    ...     [2, 3, 0, 1],
    ... ])
    >>> is_isomorphic(table1, table2)
    True
    >>> is_isomorphic(table1, table2, mapping=True)
    (1, 0, 3, 2)

    """
    from itertools import permutations
    for permutation in permutations(range(len(table1))):
        if is_isomorphism(permutation, table1, table2):
            if mapping:
                return permutation
            else:
                return True

    return False

