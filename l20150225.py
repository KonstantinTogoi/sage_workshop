def primes(n):
    """
    Return the factorization of ``n``.
    The result type is dict: {prime_factor : degree}.
    """
    n = abs(n)

    if n == 0:
        return {}
    if n == 1:
        return {1: 0}

    divisors = []
    divisor = 2
    while divisor != n:
        if n % divisor == 0:
            n = n // divisor
            divisors.append(divisor)
        else:
            divisor += 1
    divisors.append(n)

    distinct_primes = set(divisors)
    result = {}

    for divisor in distinct_primes:
        result[divisor] = divisors.count(divisor)

    return result


def solve_quadratic(a, b, c):
    """
    Return distinct roots of quadratic equation ``a*x^2 + b*x + c``.
    If a = b = c = 0 then return None.
    """
    if a == 0 and b == 0 and c == 0:
        return None

    D = b*b - 4*a*c
    vertex = -b/2/a

    if D == 0:
        return vertex

    m = (D**0.5)/2/a

    return (vertex - m, vertex + m)


def evaluate(coeffs, t):
    """
    Evaluates polynomial ``p(x)=a0 + a1*x + ... + an*x^n``
    for ``x`` equal to ``t``.

    Parameters
    ==========

    coeffs : list of integers [a0, a1, ... , an]
    t : float

    """
    from functools import reduce
    addends = []

    x = 1
    for coeff in coeffs:
        addends.append(coeff * x)
        x *= t

    return reduce(lambda x, y: x + y, addends)


def factors(n):
    """
    All integer factors of integer ``n``.

    Examples
    ========
    >>> from l20150225 import factors
    >>> factors(438)
    [-438, -219, -146, -73, -6, -3, -2, -1, 1, 2, 3, 6, 73, 146, 219, 438]
    >>> factors(-9)
    [-9, -3, -1, 1, 3, 9]

    """
    from itertools import product
    from functools import reduce
    result = []

    f = primes(n)
    if f == {}:
        return [0]
    prime_factors = f.keys()
    powers = f.values()
    ranges = list(map(range, [p+1 for p in powers]))

    for p in product(*ranges):
        ff = list(map(pow, prime_factors, p))
        ff = reduce(lambda x, y: x*y, ff)
        result += [-ff, ff]

    return sorted(result)


def int_roots(coeffs):
    """
    All integer roots of polynomial ``p(x)=a0 + a1*x + ... + an*x^n``.

    Parameters
    ==========

    coeffs : list of integers [a0, a1, ... , an]

    Examples
    ========
    >>> from l20150225 import int_roots
    >>> int_roots([60, -23, -2, 1])
    [-5, 3, 4]
    >>> int_roots([-1, 0, 1])
    [-1, 1]

    """
    result = []

    n = len(coeffs)
    a0 = coeffs[0]
    an = coeffs[-1]

    for p in factors(a0):
        for q in factors(an):
            if p%q == 0:
                pq = p // q
                if evaluate(coeffs, pq) == 0:
                    result.append(pq)

    return sorted(set(result))

